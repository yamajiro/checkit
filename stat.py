import sys
import json
import os
import copy
import math

import numpy as np
import pystan
from matplotlib.figure import Figure
import matplotlib.pyplot as plt
from scipy.stats import gaussian_kde

import psis

def statindex(fit, index):
    fig = Figure()
    ax = fig.add_subplot(111)
    result = fit.extract()[index]
    ax.hist(result)
    ax.set(xlabel=index)
    return {
        "plot": fig,
        "average": np.average(result),
        "5pertile": np.percentile(result,5),
        "median": np.percentile(result,50),
        "95pertile": np.percentile(result,95),
    }

# aicscale=False : Original definition
# aicscale=True : scaled by 2n (For comparison to AIC, BIC and so on.)
def statWAIC(fit,aicscale=True, loglikprefix = 'log_lik'):
    log_lik = fit.extract()[loglikprefix]
    if aicscale == True:
        lppd = 2*np.log(np.exp(log_lik).mean(axis=0)).sum()
        p_waic = 2*np.var(log_lik,axis=0).sum()
    else:
        lppd = np.log(np.exp(log_lik).mean(axis=0)).mean()
        p_waic = np.var(log_lik,axis=0).mean()
    return p_waic - lppd

# WARNING!!!
# fit requires reverce temperature = 1/log(n)
def statWBIC(fit,aicscale=True, loglikprefix = 'log_lik'):
    log_lik = fit.extract()[loglikprefix]
    if aicscale == True:
        return -2*np.sum(log_lik)
    else:
        return -1*np.mean(log_lik)

def statLOO(fit, loglikprefix = 'log_lik'):
    return psis.psisloo(fit.extract()[loglikprefix])

def plotpair(fit, xaxis, yaxis, nbin=100):
    xdata = fit.extract()[xaxis]
    ydata = fit.extract()[yaxis]
    pairplot= Figure()
    xhist = pairplot.add_subplot(221)
    yhist = pairplot.add_subplot(224)
    xyscatter = pairplot.add_subplot(223)
    yxscatter = pairplot.add_subplot(222)
    xhist.hist(xdata)
    xhist.set_title(xaxis)
    yhist.hist(ydata)
    yhist.set_title(yaxis)
    xyscatter.hist2d(xdata, ydata, bins=nbin, normed=True)
    yxscatter.hist2d(ydata, xdata, bins=nbin, normed=True)
    pairplot.tight_layout()

    # Heatmap
    heatmap = Figure()
    heat = heatmap.add_subplot(111)

    xx, yy = np.mgrid[
      np.min(xdata):np.max(xdata):(np.max(xdata)-np.min(xdata))/nbin,
      np.min(ydata):np.max(ydata):(np.max(ydata)-np.min(ydata))/nbin
    ]

    positions = np.vstack([xx.ravel(), yy.ravel()])
    value = np.vstack([xdata, ydata])
    kernel = gaussian_kde(value)

    f = np.reshape(kernel(positions).T, xx.shape)
    f = f / np.max(f)
    heat.contourf(
      xx, yy, f,
      levels=np.arange(0, 1, 0.1),
    )
    cont = heat.contour(
      xx, yy, f,
      linewidths=1,
      levels=np.arange(0, 1, 0.1),
      colors=['black']
    )
    cont.clabel(
      cont.levels[::2],
      fontsize=8,
    )
    return pairplot,heatmap

def plotrunavg(fit,index):
    fig = Figure()
    ax = fig.add_subplot(111)
    ax.set(xlabel=("Running Average in "+index))
    res = fit.extract(permuted=False, inc_warmup=False)
    res = res[:,:,fit.flatnames.index(index)]
    pltx = np.arange(1,np.shape(res)[0]+1)
    for x in range(10):
        ax.plot(res[:,x].cumsum()/pltx)

    return fig

def plotgelman(fit, index, batchsize=10, batchmax=50):
    data = fit.extract(pars=index,permuted=False, inc_warmup=True)[index]
    print(",".join([str(data.shape[0]),str(data.shape[1])]))
    nbat = min(data.shape[0]-50, batchmax)
    batchsize = math.floor((data.shape[0]-50)/nbat)
    x = range(50, data.shape[0], batchsize)
    R = [rscore(np.array(data[0:i][:]).T, i) for i in x]
    fig = Figure()
    gelmanplot = fig.add_subplot(111)
    gelmanplot.plot(x, R)
    gelmanplot.hlines(y=1, xmin=0, xmax=data.shape[0])
    return fig

def rscore(x, num_samples):
    # from diagostics.py in PyMC3
    # print(np.mean(x, axis=1))
    # print(x.shape)
    # Calculate between-chain variance
    B = num_samples * np.var(np.mean(x, axis=1), axis=0, ddof=1)

    # Calculate within-chain variance
    W = np.mean(np.var(x, axis=1, ddof=1), axis=0)

    # Estimate of marginal posterior variance
    Vhat = W * (num_samples - 1) / num_samples + B / num_samples

    return math.sqrt((Vhat/W))  
